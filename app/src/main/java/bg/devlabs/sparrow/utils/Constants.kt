package bg.devlabs.sparrow.utils


/**
 * Created by Radoslav Yankov on 02.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
const val CLIENT_ID = "59f8d65d73fa98158c5732ed"
const val RC_SIGN_IN: Int = 100

enum class ContextAction {
    SIGNUP,
    MAIN,
    SETTINGS
}

enum class ContextPlaid {
    SIGNUP,
    FUNDING,
    CONNECTED_ADD,
    CONNECTED_REMOVE
}

const val SIGNUP_COMPLETE = "0"
const val SETUP_FUNDING_COMPLETE = "1"
const val CAUSES_COMPLETE = "2"
const val CORE_VALUES_COMPLETE = "3"
