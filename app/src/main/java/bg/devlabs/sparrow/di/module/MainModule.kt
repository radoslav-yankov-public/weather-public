package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.main.MainContract
import bg.devlabs.sparrow.ui.main.MainFragment
import bg.devlabs.sparrow.ui.main.MainPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 08.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class MainModule {
    @Binds
    abstract fun provideView(fragment: MainFragment): MainContract.View

    @Binds
    abstract fun providePresenter(presenter: MainPresenter): MainContract.Presenter
}