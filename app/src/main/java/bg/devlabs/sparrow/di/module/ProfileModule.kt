package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.profile.ProfileContract
import bg.devlabs.sparrow.ui.profile.ProfileFragment
import bg.devlabs.sparrow.ui.profile.ProfilePresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class ProfileModule {
    @Binds
    abstract fun provideView(fragment: ProfileFragment): ProfileContract.View

    @Binds
    abstract fun providePresenter(presenter: ProfilePresenter): ProfileContract.Presenter
}