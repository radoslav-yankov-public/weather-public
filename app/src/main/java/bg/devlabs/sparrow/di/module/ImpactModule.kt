package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.impact.ImpactContract
import bg.devlabs.sparrow.ui.impact.ImpactFragment
import bg.devlabs.sparrow.ui.impact.ImpactPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class ImpactModule {
    @Binds
    abstract fun provideView(fragment: ImpactFragment): ImpactContract.View

    @Binds
    abstract fun providePresenter(presenter: ImpactPresenter): ImpactContract.Presenter
}