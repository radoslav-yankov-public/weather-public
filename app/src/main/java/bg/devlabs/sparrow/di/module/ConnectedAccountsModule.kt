package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.connectedaccounts.ConnectedAccountsContract
import bg.devlabs.sparrow.ui.connectedaccounts.ConnectedAccountsFragment
import bg.devlabs.sparrow.ui.connectedaccounts.ConnectedAccountsPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class ConnectedAccountsModule {
    @Binds
    abstract fun provideView(fragment: ConnectedAccountsFragment): ConnectedAccountsContract.View

    @Binds
    abstract fun providePresenter(presenter: ConnectedAccountsPresenter): ConnectedAccountsContract.Presenter
}