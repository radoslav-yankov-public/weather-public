package bg.devlabs.sparrow.di

import android.view.LayoutInflater

import bg.devlabs.sparrow.ui.base.BaseActivity
import dagger.Module
import dagger.Provides

/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */
@Module
class BaseActivityModule {

    @Provides
    internal fun provideLayoutInflater(activity: BaseActivity): LayoutInflater {
        return activity.layoutInflater
    }
}