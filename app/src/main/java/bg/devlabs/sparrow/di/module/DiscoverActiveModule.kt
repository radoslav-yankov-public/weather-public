package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.discoveractive.DiscoverActiveContract
import bg.devlabs.sparrow.ui.discoveractive.DiscoverActiveFragment
import bg.devlabs.sparrow.ui.discoveractive.DiscoverActivePresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class DiscoverActiveModule {
    @Binds
    abstract fun provideView(fragment: DiscoverActiveFragment): DiscoverActiveContract.View

    @Binds
    abstract fun providePresenter(presenter: DiscoverActivePresenter): DiscoverActiveContract.Presenter
}