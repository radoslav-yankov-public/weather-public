package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.funding.FundingAccountContract
import bg.devlabs.sparrow.ui.funding.FundingAccountFragment
import bg.devlabs.sparrow.ui.funding.FundingAccountPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class FundingAccountModule {
    @Binds
    abstract fun provideView(fragment: FundingAccountFragment): FundingAccountContract.View

    @Binds
    abstract fun providePresenter(presenter: FundingAccountPresenter): FundingAccountContract.Presenter
}