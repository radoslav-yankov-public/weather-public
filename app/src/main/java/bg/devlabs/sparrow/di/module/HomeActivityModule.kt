package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.home.HomeActivity
import bg.devlabs.sparrow.ui.home.HomeContract
import bg.devlabs.sparrow.ui.home.HomePresenter
import dagger.Binds
import dagger.Module

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

@Module
abstract class HomeActivityModule {
    @Binds
    abstract fun provideView(activity: HomeActivity): HomeContract.View

    @Binds
    abstract fun providePresenter(presenter: HomePresenter): HomeContract.Presenter
}
