package bg.devlabs.sparrow.di.module


import android.app.Application
import android.content.Context
import bg.devlabs.sparrow.BuildConfig
import bg.devlabs.sparrow.data.AppDataManager
import bg.devlabs.sparrow.data.DataManager
import bg.devlabs.sparrow.data.auth.AppAuthHelper
import bg.devlabs.sparrow.data.auth.AuthHelper
import bg.devlabs.sparrow.data.network.ApiKey
import bg.devlabs.sparrow.data.network.AppNetworkHelper
import bg.devlabs.sparrow.data.network.NetworkHelper
import bg.devlabs.sparrow.data.prefs.AppPreferencesHelper
import bg.devlabs.sparrow.data.prefs.PreferencesHelper
import bg.devlabs.sparrow.di.ApplicationContext
import bg.devlabs.sparrow.di.PreferenceInfo
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

@Module
internal class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    @ApplicationContext
    fun provideActualContext(application: Context): Context = application

    @Provides
    @Singleton
    internal fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        //		gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    internal fun provideOkhttpClient(cache: Cache): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.cache(cache)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(logging)

        return client.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @ApiKey
    internal fun provideApiKey(): String = BuildConfig.API_KEY

    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String = "sparrow_pref"

    @Provides
    @Singleton
    fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }

    @Provides
    @Singleton
    fun provideNetworkHelper(appNetworkHelper: AppNetworkHelper): NetworkHelper {
        return appNetworkHelper
    }

    @Provides
    @Singleton
    fun providePreferencesHelper(appPreferencesHelper: AppPreferencesHelper): PreferencesHelper {
        return appPreferencesHelper
    }

    @Provides
    fun provideGoogleSignInOptions(): GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()

    @Provides
    fun provideGoogleSignInClient(context: Context, googleSignInOptions: GoogleSignInOptions)
            : GoogleSignInClient = GoogleSignIn.getClient(context, googleSignInOptions)

    @Provides
    fun provideFacebookCallbackManager(): CallbackManager = CallbackManager.Factory.create()

    @Provides
    @Singleton
    fun provideAuthHelper(appAuthHelper: AppAuthHelper): AuthHelper {
        return appAuthHelper
    }

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()
}