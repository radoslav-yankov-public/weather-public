package bg.devlabs.sparrow.di

import bg.devlabs.sparrow.di.module.*
import bg.devlabs.sparrow.ui.WelcomeFragment
import bg.devlabs.sparrow.ui.causes.CausesFragment
import bg.devlabs.sparrow.ui.connectedaccounts.ConnectedAccountsFragment
import bg.devlabs.sparrow.ui.corevalues.CoreValuesFragment
import bg.devlabs.sparrow.ui.demo.DemoFragment
import bg.devlabs.sparrow.ui.discoveractive.DiscoverActiveFragment
import bg.devlabs.sparrow.ui.funding.FundingAccountFragment
import bg.devlabs.sparrow.ui.home.HomeActivity
import bg.devlabs.sparrow.ui.impact.ImpactFragment
import bg.devlabs.sparrow.ui.main.MainFragment
import bg.devlabs.sparrow.ui.plaid.PlaidFragment
import bg.devlabs.sparrow.ui.profile.ProfileFragment
import bg.devlabs.sparrow.ui.settings.SettingsFragment
import bg.devlabs.sparrow.ui.signin.SignInFragment
import bg.devlabs.sparrow.ui.signup.SignUpFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

@Module
internal abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    internal abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [PlaidModule::class])
    internal abstract fun contributePlaidFragment(): PlaidFragment

    @ContributesAndroidInjector(modules = [SignUpModule::class])
    internal abstract fun contributeSignUpFragment(): SignUpFragment

    @ContributesAndroidInjector(modules = [SignInModule::class])
    internal abstract fun contributeSignInFragment(): SignInFragment

    @ContributesAndroidInjector(modules = [DemoModule::class])
    internal abstract fun contributeDemoFragment(): DemoFragment

    @ContributesAndroidInjector(modules = [CausesModule::class])
    internal abstract fun contributeCausesFragment(): CausesFragment

    @ContributesAndroidInjector(modules = [CoreValuesModule::class])
    internal abstract fun contributeCoreValuesFragment(): CoreValuesFragment

    @ContributesAndroidInjector(modules = [DiscoverActiveModule::class])
    internal abstract fun contributeDiscoverActiveFragment(): DiscoverActiveFragment

    @ContributesAndroidInjector(modules = [MainModule::class])
    internal abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector(modules = [ImpactModule::class])
    internal abstract fun contributeImpactFragment(): ImpactFragment

    @ContributesAndroidInjector(modules = [SettingsModule::class])
    internal abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector(modules = [ProfileModule::class])
    internal abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector(modules = [FundingAccountModule::class])
    internal abstract fun contributeFundingAccountFragment(): FundingAccountFragment

    @ContributesAndroidInjector(modules = [ConnectedAccountsModule::class])
    internal abstract fun contributeConnectedAccountsFragment(): ConnectedAccountsFragment

    @ContributesAndroidInjector(modules = [WelcomeModule::class])
    internal abstract fun contributeWelcomeFragment(): WelcomeFragment
}
