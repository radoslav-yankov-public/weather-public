package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.signin.SignInContract
import bg.devlabs.sparrow.ui.signin.SignInFragment
import bg.devlabs.sparrow.ui.signin.SignInPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class SignInModule {
    @Binds
    abstract fun provideView(fragment: SignInFragment): SignInContract.View

    @Binds
    abstract fun providePresenter(presenter: SignInPresenter): SignInContract.Presenter
}