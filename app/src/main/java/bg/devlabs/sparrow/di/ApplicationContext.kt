package bg.devlabs.sparrow.di

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Qualifier


/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class ApplicationContext

annotation class PreferenceInfo