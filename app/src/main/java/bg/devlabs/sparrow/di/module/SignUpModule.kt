package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.signup.SignUpContract
import bg.devlabs.sparrow.ui.signup.SignUpFragment
import bg.devlabs.sparrow.ui.signup.SignUpPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class SignUpModule {
    @Binds
    abstract fun provideView(fragment: SignUpFragment): SignUpContract.View

    @Binds
    abstract fun providePresenter(presenter: SignUpPresenter): SignUpContract.Presenter
}