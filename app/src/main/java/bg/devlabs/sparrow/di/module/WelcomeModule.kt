package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.WelcomeFragment
import bg.devlabs.sparrow.ui.welcome.WelcomeContract
import bg.devlabs.sparrow.ui.welcome.WelcomePresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 20.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class WelcomeModule {
    @Binds
    abstract fun provideView(fragment: WelcomeFragment): WelcomeContract.View

    @Binds
    abstract fun providePresenter(presenter: WelcomePresenter): WelcomeContract.Presenter
}