package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.causes.CausesContract
import bg.devlabs.sparrow.ui.causes.CausesFragment
import bg.devlabs.sparrow.ui.causes.CausesPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class CausesModule {
    @Binds
    abstract fun provideView(fragment: CausesFragment): CausesContract.View

    @Binds
    abstract fun providePresenter(presenter: CausesPresenter): CausesContract.Presenter
}