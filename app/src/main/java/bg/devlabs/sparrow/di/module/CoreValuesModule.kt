package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.corevalues.CoreValuesContract
import bg.devlabs.sparrow.ui.corevalues.CoreValuesFragment
import bg.devlabs.sparrow.ui.corevalues.CoreValuesPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class CoreValuesModule {
    @Binds
    abstract fun provideView(fragment: CoreValuesFragment): CoreValuesContract.View

    @Binds
    abstract fun providePresenter(presenter: CoreValuesPresenter): CoreValuesContract.Presenter
}