package bg.devlabs.sparrow.data.network

import bg.devlabs.sparrow.data.network.model.CredentialsRequest
import bg.devlabs.sparrow.data.network.model.CredentialsResponse
import bg.devlabs.sparrow.data.network.model.UpdateUserRequest
import bg.devlabs.sparrow.data.network.model.bank.BankInfoRequest
import bg.devlabs.sparrow.data.network.model.login.LoginResponse
import bg.devlabs.sparrow.data.network.model.userRead.UserRead
import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.data.network.model.userscriptsparam.UserScriptsParamResponse
import bg.devlabs.sparrow.data.network.model.values.ValuesResponse
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.*

/**
 * Created by Radoslav Yankov on 21.11.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
interface ApiService {
    @GET("/api/user/{userId}/read")
    fun getUser(
            @Path("userId") userId: String,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<Result<UserRead>>

    @GET("/api/user-data/{userId}/read")
    fun getUserData(
            @Path("userId") userId: String,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<Result<UserDataResponse>>

    @GET("/api/event/roots")
    fun getCoreValues(
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<Result<ValuesResponse>>

//    @PATCH("{fullUrl}") fun sendGeoFencingData(
//            @Path(value = "fullUrl", encoded = true) fullUrl: String,
//            @Header("x-auth") authToker: String,
//            @Body data: GeoFencingApiObject): Observable<Response<Unit>>

    @POST("/api/user/create") fun createUser(
            @Body data: CredentialsRequest,
            @Header("auth-client") clientId: String): Observable<Result<CredentialsResponse>>

    @PATCH("/api/user-data/{userId}/update") fun updateUser(
            @Path("userId") userId: String,
            @Body data: UpdateUserRequest,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<retrofit2.Response<Unit>>

    @PATCH("/api/user-scripts-param/{userId}/update") fun updateBankInfo(
            @Path("userId") userId: String,
            @Body data: BankInfoRequest,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<retrofit2.Response<Unit>>

    @PATCH("/api/event/{eventId}/ready") fun readyValue(
            @Path("eventId") eventId: String,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<retrofit2.Response<Unit>>

    @PATCH("/api/event/{eventId}/unready") fun unreadyValue(
            @Path("eventId") eventId: String,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<retrofit2.Response<Unit>>


    @PATCH("/api/user/login") fun login(
            @Body data: CredentialsRequest,
            @Header("auth-client") clientId: String): Observable<Result<LoginResponse>>


    @PATCH("/api/user/logout") fun logout(
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<Result<Unit>>


    @DELETE("/api/user/{userId}/remove") fun delete(
            @Path("userId") userId: String,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<Result<Unit>>

    @GET("/api/user-scripts-param/{userId}/read") fun getUserScriptsParam(
            @Path("userId") userId: String,
            @Header("auth-client") clientId: String,
            @Header("auth-user") xAuth: String): Observable<Result<UserScriptsParamResponse>>

    @PATCH("/api/user/{userId}/status") fun setStatus(
            @Path("userId") userId: String,
            @Header("auth-user") xAuth: String,
            @Body data: CredentialsRequest,
            @Header("auth-client") clientId: String): Observable<Result<Unit>>

}
