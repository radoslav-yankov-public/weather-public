package bg.devlabs.sparrow.data.network.model.bank

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Plaid(
        @SerializedName("public_token")
        @Expose
        var publicToken: String,
        @SerializedName("account_id")
        @Expose
        var accountId: String = "",
        @SerializedName("account_type")
        @Expose
        var accountType: String,
        @SerializedName("institution_id")
        @Expose
        var institutionId: String = "",
        @SerializedName("accounts")
        @Expose
        var accounts: String,
        @SerializedName("institution_name")
        @Expose
        var bankName: String,
        @SerializedName("account_name")
        @Expose
        var accountName: String,
        @SerializedName("name")
        @Expose
        var name: String,
        @SerializedName("_id")
        @Expose
        var id: String? = null

)
