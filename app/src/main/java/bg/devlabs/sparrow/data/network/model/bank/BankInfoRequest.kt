package bg.devlabs.sparrow.data.network.model.bank

import bg.devlabs.sparrow.ui.plaid.Bank
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BankInfoRequest(bank: Bank, bank2: Bank?) {

    @SerializedName("userScriptsParam")
    @Expose
    var userScriptsParam = UserScriptsParam(bank, bank2)

}
