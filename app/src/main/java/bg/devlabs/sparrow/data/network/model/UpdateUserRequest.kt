package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateUserRequest(name: String? = null, causes: MutableList<CauseRequest>? = null) {

    @SerializedName("userData")
    @Expose
    var userData = UserData(name = name, causes = causes)

}
