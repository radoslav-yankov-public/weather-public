package bg.devlabs.sparrow.data.network.model.bank

import bg.devlabs.sparrow.ui.plaid.Bank
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Actions(bank: Bank, bank2: Bank?) {

    @SerializedName("plaid")
    @Expose
    var plaid = if (bank2 == null) {
        if (bank.id != null) {
            listOf(Plaid(bank.publicToken, bank.accountId, bank.accountType, bank.institutionId, bank.accounts, bank.bankName, bank.accountName, bank.name?:"name", bank.id))
        } else {
            listOf(Plaid(bank.publicToken, bank.accountId, bank.accountType, bank.institutionId, bank.accounts, bank.bankName, bank.accountName, "name"))
        }
    } else {
        listOf(Plaid(bank.publicToken, bank.accountId, bank.accountType, bank.institutionId, bank.accounts, bank.bankName, bank.accountName, "name"),
                Plaid(bank2.publicToken, bank2.accountId, bank2.accountType, bank2.institutionId, bank2.accounts, bank2.bankName, bank2.accountName, "name"))
    }
}
