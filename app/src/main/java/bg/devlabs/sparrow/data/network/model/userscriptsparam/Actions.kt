package bg.devlabs.sparrow.data.network.model.userscriptsparam

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Actions {

    @SerializedName("plaid")
    @Expose
    var plaid: List<Plaid>? = null

}
