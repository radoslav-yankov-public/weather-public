package bg.devlabs.sparrow.data.network.model


/**
 * Created by Slavi Petrov on 05.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class Cause(var nameResId: Int, var value: Int)