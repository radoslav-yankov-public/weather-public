package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserData(
        @SerializedName("name")
        @Expose
        var name: String? = null,
        @SerializedName("values")
        @Expose
        var causes: MutableList<CauseRequest>? = null)
