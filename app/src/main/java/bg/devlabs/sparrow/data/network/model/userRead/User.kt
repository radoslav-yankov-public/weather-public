package bg.devlabs.sparrow.data.network.model.userRead

import bg.devlabs.sparrow.utils.SIGNUP_COMPLETE
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("userId")
    @Expose
    var userId: String? = null
    @SerializedName("CLIENT_ID")
    @Expose
    var clientId: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("status")
    @Expose
    var status: String? = SIGNUP_COMPLETE

}
