package bg.devlabs.sparrow.data.auth

import android.content.Context
import android.content.Intent
import bg.devlabs.sparrow.di.ApplicationContext
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 20.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class AppAuthHelper @Inject constructor(@ApplicationContext context: Context,
                                        override val googleSignInClient: GoogleSignInClient,
                                        override val facebookCallbackManager: CallbackManager)
    : AuthHelper {
    private val googleSignInAccount = GoogleSignIn.getLastSignedInAccount(context)

    override fun isGoogleAccountSignedIn() = googleSignInAccount != null

    override fun onGoogleSignInResult(data: Intent?): Single<GoogleSignInAccount> {
        return try {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val account = task?.getResult(ApiException::class.java)
            Single.just(account)
        } catch (e: ApiException) {
            Single.error(e)
        }
    }

    override fun onFacebookSignInResult(requestCode: Int, resultCode: Int, data: Intent?) {
        facebookCallbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun registerFacebookLoginCallback(): Single<LoginResult> =
            Single.create<LoginResult> {
                LoginManager.getInstance().registerCallback(facebookCallbackManager,
                        object : FacebookCallback<LoginResult> {
                            override fun onSuccess(result: LoginResult) {
                                it.onSuccess(result)
                            }

                            override fun onCancel() {
//                                it.onError(FacebookException("User cancelled!"))
                            }

                            override fun onError(error: FacebookException) {
                                it.onError(error)
                            }
                        })
            }
}