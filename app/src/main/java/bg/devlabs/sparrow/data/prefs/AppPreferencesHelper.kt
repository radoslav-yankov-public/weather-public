package bg.devlabs.sparrow.data.prefs

import android.content.Context
import android.content.SharedPreferences
import bg.devlabs.sparrow.di.ApplicationContext
import bg.devlabs.sparrow.di.PreferenceInfo
import javax.inject.Inject


/**
 * Created by Radoslav Yankov on 22.11.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class AppPreferencesHelper @Inject
constructor(@ApplicationContext context: Context,
            @PreferenceInfo prefFileName: String) : PreferencesHelper {
    override var userId: String
        get() = mPrefs.getString(PREF_KEY_CURRENT_USER_ID, "")
        set(value) {mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID, value).apply()}
    override var xAuth: String
        get() = mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "")
        set(value) {mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, value).apply()}

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    companion object {
        private val PREF_KEY_CURRENT_USER_ID = "USER_ID"
        private val PREF_KEY_ACCESS_TOKEN = "X_AUTH"
    }
}
