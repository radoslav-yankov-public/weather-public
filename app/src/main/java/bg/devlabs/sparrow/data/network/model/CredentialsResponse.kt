package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CredentialsResponse {

    @SerializedName("user")
    @Expose
    var user: UserResponse? = null

}
