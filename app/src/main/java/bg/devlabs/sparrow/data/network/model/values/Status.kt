package bg.devlabs.sparrow.data.network.model.values

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Status {

    @SerializedName("ready")
    @Expose
    var ready: Boolean? = null

}
