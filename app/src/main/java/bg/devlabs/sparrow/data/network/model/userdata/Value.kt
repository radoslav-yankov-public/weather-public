package bg.devlabs.sparrow.data.network.model.userdata

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Radoslav Yankov on 12.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class Value(@SerializedName("name")
            @Expose var name: String?, @SerializedName("value")
            @Expose var value: String?) {

}