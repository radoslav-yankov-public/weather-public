package bg.devlabs.sparrow.data.network.model.bank

import bg.devlabs.sparrow.ui.plaid.Bank
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserScriptsParam(bank: Bank, bank2: Bank?) {

    @SerializedName("actions")
    @Expose
    var actions = Actions(bank, bank2)

}
