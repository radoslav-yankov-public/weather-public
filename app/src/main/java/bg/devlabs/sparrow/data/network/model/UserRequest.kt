package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Radoslav Yankov on 02.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
data class UserRequest(
        @SerializedName("email")
        @Expose val email: String? = null, @SerializedName("password")
        @Expose val password: String? = null, @SerializedName("status")
        @Expose val status: String? = null) {
//    @SerializedName("email")
//    @Expose
//    var email: String? = null
//    @SerializedName("password")
//    @Expose
//    var password: String? = null
}