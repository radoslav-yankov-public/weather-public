package bg.devlabs.sparrow.data

import bg.devlabs.sparrow.data.auth.AuthHelper
import bg.devlabs.sparrow.data.network.NetworkHelper
import bg.devlabs.sparrow.data.prefs.PreferencesHelper


/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

interface DataManager : NetworkHelper, PreferencesHelper, AuthHelper {
    fun isUserLoggedIn(): Boolean
}
