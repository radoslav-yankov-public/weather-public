package bg.devlabs.sparrow.data.network.model.userscriptsparam

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserScriptsParam {

    @SerializedName("userId")
    @Expose
    var userId: String? = null
    @SerializedName("userScriptsParam")
    @Expose
    var userScriptsParam: UserScriptsParam_? = null

}
