package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserResponse {

    @SerializedName("userId")
    @Expose
    var userId: String? = null
    @SerializedName("CLIENT_ID")
    @Expose
    var clientId: String? = null
    @SerializedName("groupId")
    @Expose
    var groupId: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null

}
