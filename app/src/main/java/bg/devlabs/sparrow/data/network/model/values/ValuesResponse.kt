package bg.devlabs.sparrow.data.network.model.values

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ValuesResponse {

    @SerializedName("events")
    @Expose
    var events: List<Event>? = null

}
