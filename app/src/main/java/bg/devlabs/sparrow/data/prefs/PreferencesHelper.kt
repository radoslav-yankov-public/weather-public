package bg.devlabs.sparrow.data.prefs



/**
 * Created by Radoslav Yankov on 22.11.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
interface PreferencesHelper{
    var userId: String
    var xAuth: String
}