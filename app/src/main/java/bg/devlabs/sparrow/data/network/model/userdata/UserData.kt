package bg.devlabs.sparrow.data.network.model.userdata

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserData {

    @SerializedName("userId")
    @Expose
    var userId: String? = null
    @SerializedName("userData")
    @Expose
    var userData: UserData_? = null

}
