package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Radoslav Yankov on 09.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
data class CauseRequest(
        @SerializedName("name")
        @Expose
        private var name: String? = null,
        @SerializedName("value")
        @Expose
        private var value: String? = null
)
