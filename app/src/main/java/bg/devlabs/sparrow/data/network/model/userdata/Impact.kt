package bg.devlabs.sparrow.data.network.model.userdata

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Impact {

    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("value")
    @Expose
    var value: String? = null

}
