package bg.devlabs.sparrow.data

import android.content.Intent
import bg.devlabs.sparrow.data.auth.AuthHelper
import bg.devlabs.sparrow.data.network.NetworkHelper
import bg.devlabs.sparrow.data.prefs.PreferencesHelper
import bg.devlabs.sparrow.ui.plaid.Bank
import com.facebook.CallbackManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

class AppDataManager @Inject
constructor(private val networkHelper: NetworkHelper,
            private val preferencesHelper: PreferencesHelper,
            private val authHelper: AuthHelper) : DataManager {
    override val googleSignInClient: GoogleSignInClient
        get() = authHelper.googleSignInClient

    override val facebookCallbackManager: CallbackManager
        get() = authHelper.facebookCallbackManager

    override var userId: String
        get() = preferencesHelper.userId
        set(value) {
            preferencesHelper.userId = value
        }
    override var xAuth: String
        get() = preferencesHelper.xAuth
        set(value) {
            preferencesHelper.xAuth = value
        }

    override fun isUserLoggedIn(): Boolean {
        val emailAccountSignedIn = userId != "" && xAuth != ""
        val googleAccountSignedIn = authHelper.isGoogleAccountSignedIn()
        return emailAccountSignedIn || googleAccountSignedIn
    }

    override fun isGoogleAccountSignedIn(): Boolean = authHelper.isGoogleAccountSignedIn()
    override fun onGoogleSignInResult(data: Intent?): Single<GoogleSignInAccount> =
            authHelper.onGoogleSignInResult(data).prepare()

    override fun onFacebookSignInResult(requestCode: Int, resultCode: Int, data: Intent?) =
            authHelper.onFacebookSignInResult(requestCode, resultCode, data)

    override fun registerFacebookLoginCallback(): Single<LoginResult>? =
            authHelper.registerFacebookLoginCallback()?.prepare()

    override fun createUser(email: String, password: String) = networkHelper.createUser(email, password).prepare()
    override fun login(email: String, password: String) = networkHelper.login(email, password).prepare()
    override fun updateUser(userId: String, xAuth: String, updatedData: Any) = networkHelper.updateUser(userId, xAuth, updatedData).prepare()
    override fun updateBankInfo(userId: String, xAuth: String, bank: Bank, bank2: Bank) =
            networkHelper.updateBankInfo(userId, xAuth, bank, bank2).prepare()

    override fun getUser(userId: String, xAuth: String) = networkHelper.getUser(userId, xAuth).prepare()
    override fun getUserData(userId: String, xAuth: String) = networkHelper.getUserData(userId, xAuth).prepare()
    override fun getCoreValues(xAuth: String) = networkHelper.getCoreValues(xAuth).prepare()
    override fun readyValue(eventId: String, xAuth: String) = networkHelper.readyValue(eventId, xAuth).prepare()
    override fun unreadyValue(eventId: String, xAuth: String) = networkHelper.unreadyValue(eventId, xAuth).prepare()
    override fun logout(xAuth: String) = networkHelper.logout(xAuth).prepare()
    override fun delete(userId: String, xAuth: String) = networkHelper.delete(userId, xAuth).prepare()
    override fun getUserScriptsParam(userId: String, xAuth: String) = networkHelper.getUserScriptsParam(userId, xAuth).prepare()
    override fun setStatus(userId: String, xAuth: String, status: String) = networkHelper.setStatus(userId, xAuth, status).prepare()

    private fun <T> Observable<T>.prepare() = this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    private fun <T> Single<T>.prepare() = this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}