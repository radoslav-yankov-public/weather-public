package bg.devlabs.sparrow.data.network.model.userdata

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserDataResponse {

    @SerializedName("userData")
    @Expose
    var userData: UserData? = null

}
