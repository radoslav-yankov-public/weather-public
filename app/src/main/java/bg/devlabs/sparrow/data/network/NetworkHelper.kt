package bg.devlabs.sparrow.data.network

import bg.devlabs.sparrow.data.network.model.CredentialsResponse
import bg.devlabs.sparrow.data.network.model.login.LoginResponse
import bg.devlabs.sparrow.data.network.model.userRead.UserRead
import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.data.network.model.userscriptsparam.UserScriptsParamResponse
import bg.devlabs.sparrow.data.network.model.values.ValuesResponse
import bg.devlabs.sparrow.ui.plaid.Bank
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.Result

/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

interface NetworkHelper {
    fun createUser(email: String, password: String): Observable<Result<CredentialsResponse>>
    fun login(email: String, password: String): Observable<Result<LoginResponse>>
    fun getUser(userId: String, xAuth: String): Observable<Result<UserRead>>
    fun getUserData(userId: String, xAuth: String): Observable<Result<UserDataResponse>>
    fun getCoreValues(xAuth: String): Observable<Result<ValuesResponse>>
    fun updateUser(userId: String, xAuth: String, updatedData: Any): Observable<retrofit2.Response<Unit>>
    fun updateBankInfo(userId: String, xAuth: String, bank: Bank, bank2: Bank): Observable<retrofit2.Response<Unit>>
    fun readyValue(eventId: String, xAuth: String): Observable<retrofit2.Response<Unit>>
    fun unreadyValue(eventId: String, xAuth: String): Observable<retrofit2.Response<Unit>>
    fun logout(xAuth: String): Observable<Result<Unit>>
    fun delete(userId: String, xAuth: String): Observable<Result<Unit>>
    fun getUserScriptsParam(userId: String, xAuth: String): Observable<Result<UserScriptsParamResponse>>
    fun setStatus(userId: String, xAuth: String, status: String): Observable<Result<Unit>>
}