package bg.devlabs.sparrow.ui.corevalues

import bg.devlabs.sparrow.ui.base.BasePresenter
import bg.devlabs.sparrow.utils.CORE_VALUES_COMPLETE
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class CoreValuesPresenter @Inject constructor(view: CoreValuesContract.View)
    : BasePresenter<CoreValuesContract.View>(view), CoreValuesContract.Presenter {

    override fun onViewBind() {}

    override fun onListLoad() {
//        dataManager.xAuth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YTdjMjYyNTVkY2Y1ODM0NDA2N2ZiMGEiLCJhY2Nlc3MiOiJ4LWF1dGgiLCJpYXQiOjE1MTgwODU2NzB9.HuPgwSvR-lasavFk8T7YgoCxJKGTgoqONazltWt440s"
//        dataManager.userId = "5a7c253d5dcf58344067faf9"

        compositeDisposable.add(
                dataManager.getCoreValues(dataManager.xAuth)
                        .subscribe({
                            if (it.response()?.isSuccessful == true && it.response()?.body() != null) {
                                view?.updateValuesList(it.response()?.body()?.events)
                            } else {
                                view?.showErrorToast()
                            }
                        }, {
                            view?.showErrorToast()
                        })
        )


    }

    override fun saveOnboarding() {
        compositeDisposable.add(
                dataManager.setStatus(dataManager.userId, dataManager.xAuth, CORE_VALUES_COMPLETE)
                        .subscribe({}, {})
        )
    }
}
