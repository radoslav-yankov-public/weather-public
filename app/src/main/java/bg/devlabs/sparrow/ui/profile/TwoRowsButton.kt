package bg.devlabs.sparrow.ui.profile

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

import bg.devlabs.sparrow.R

/**
 * Created by Simona Stoyanova on 2/9/18.
 * simona@devlabs.bg
 */

class TwoRowsButton : LinearLayout {
    private var mainText: String? = null
    private var secondaryText: String? = null
    private var mainTextView: TextView? = null
    private var secondaryTextView: TextView? = null

    private fun inflate() {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = inflater.inflate(R.layout.two_rows_button, this, true)
        mainTextView = root.findViewById(R.id.main_text_view)
        secondaryTextView = root.findViewById(R.id.secondary_text_view)

        mainTextView?.text = mainText
        secondaryTextView?.text = secondaryText
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet) {
        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.TwoRowsButton,
                0, 0)

        try {
            mainText = a.getString(R.styleable.TwoRowsButton_mainText)
            secondaryText = a.getString(R.styleable.TwoRowsButton_secondaryText)
        } finally {
            a.recycle()
        }
        inflate()
    }

    //not used constructor
    constructor(context: Context) : super(context) {}
}
