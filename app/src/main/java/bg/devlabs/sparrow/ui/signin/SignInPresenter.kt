package bg.devlabs.sparrow.ui.signin

import bg.devlabs.sparrow.data.network.model.login.LoginResponse
import bg.devlabs.sparrow.ui.base.BasePresenter
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SignInPresenter @Inject constructor(view: SignInContract.View) :
        BasePresenter<SignInContract.View>(view), SignInContract.Presenter {

    override fun onSignInButtonClicked(email: String, password: String) {
        compositeDisposable.add(dataManager.login(email, password)
                .subscribe({ response: Result<LoginResponse> ->
                    if (response.response()?.code()?.toString()?.startsWith("2") == true) {
                        dataManager.xAuth = response.response()?.headers()?.get("auth-user") ?: ""
                        dataManager.userId = response.response()?.body()?.user?.userId ?: ""
                        view?.onSignInSuccess()

                    } else {
                        view?.onSignInFail()
                    }
//                    dataManager.getUser(dataManager.userId, dataManager.xAuth)
//                            .subscribe({
//                                dataManager.userId = it.response()?.body()?.user?.userId ?: ""
//                            }, {
//                                view?.showToast("Error. Please try again")
//                            })

//                    Log.d("RESPONSE", response.toString())
                }, {
                    view?.showToast("Error. Please try again")
//                    Log.d("RESPONSE", error.toString())
                    view?.onSignInFail()
                })
        )
    }

    override fun onForgotPasswordClicked() {
        // TODO: Add forgotten password call to the server.
    }
}