package bg.devlabs.sparrow.ui.connectedaccounts

import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.ui.base.BaseContract


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class ConnectedAccountsContract {
    interface View : BaseContract.View {
        fun onAccountsResolved(banks: MutableList<Plaid>)

        fun onRemoveClick(bank: Plaid, position: Int)
    }

    interface Presenter : BaseContract.Presenter {

        fun onViewBind()

        fun onChangeClick(bank: Plaid, position: Int)

        fun onAddBankClick()
    }
}