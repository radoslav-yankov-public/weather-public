package bg.devlabs.sparrow.ui.impact

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import kotlinx.android.synthetic.main.fragment_impact.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class ImpactFragment : InjectionBaseFragment(), ImpactContract.View {
    @Inject
    lateinit var presenter: ImpactContract.Presenter
    lateinit var adapter: ImpactAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_impact, container, false)
    }

    override fun onDataFetched(data: UserDataResponse?) {
        val impact = data?.userData?.userData?.impact
        if (impact == null || impact.isEmpty()) {
            impact_empty_page_text_view.visibility = View.VISIBLE
            return
        }

        adapter = ImpactAdapter(presenter, data)
        impact_recycler_view.adapter = adapter
        impact_recycler_view.layoutManager = LinearLayoutManager(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.fetchImpact()
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }
}