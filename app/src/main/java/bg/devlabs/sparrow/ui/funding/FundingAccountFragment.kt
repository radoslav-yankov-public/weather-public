package bg.devlabs.sparrow.ui.funding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.plaid.PlaidFragment
import bg.devlabs.sparrow.utils.ContextPlaid
import bg.devlabs.sparrow.utils.onClick
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.item_bank.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class FundingAccountFragment : InjectionBaseFragment(), FundingAccountContract.View {
    @Inject
    lateinit var presenter: FundingAccountContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_funding_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.setActionBarTitle(getString(R.string.text_funding_account))
        presenter.onViewBind()
    }

    override fun onAccountResolved(plaid: Plaid) {
        text_bank_name.text = plaid.institutionName
        text_card_number.text = plaid.accounts?.substringAfter("number")?.substring(3, 7)
        text_change_card.onClick {
            showFragment(PlaidFragment().apply {
                setData(plaid = plaid)
                contextPlaid = ContextPlaid.FUNDING
            }, R.string.plaid_fragment)
//            presenter.onChangeClick()
        }
    }

    override fun onAccountChanged() {

    }


    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

}