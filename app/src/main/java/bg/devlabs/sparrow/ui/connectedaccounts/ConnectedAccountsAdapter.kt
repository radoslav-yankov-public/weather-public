package bg.devlabs.sparrow.ui.connectedaccounts


/**
 * Created by Radoslav Yankov on 07.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.*
import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_bank.*


class ConnectedAccountsAdapter(var presenter: ConnectedAccountsContract.Presenter, var data: MutableList<Plaid>?) :
        RecyclerView.Adapter<ConnectedAccountsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val containerView = inflater.inflate(R.layout.item_bank, parent, false)
        return ViewHolder(presenter, containerView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setup(data?.get(position), position)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }


    class ViewHolder(val presenter: ConnectedAccountsContract.Presenter, override val containerView: View?) :
            RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun setup(item: Plaid?, currentPosition: Int = 0) {
            text_bank_name?.text = item?.institutionName
            text_card_number?.text = ""
            if (item?.accountId == "") {
                label_card_number?.text = ""
                text_change_card?.text = text_change_card.context.getString(R.string.text_remove)
                text_change_card.onClick {
                    presenter.onChangeClick(item, currentPosition)
                }
            } else {
                label_card_number?.text = label_card_number.context.getString(R.string.text_main_account)
                text_change_card?.text = ""
                text_change_card.setOnClickListener(null)
            }
        }

    }

}