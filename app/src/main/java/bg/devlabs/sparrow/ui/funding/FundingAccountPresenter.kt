package bg.devlabs.sparrow.ui.funding

import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.data.network.model.userscriptsparam.UserScriptsParamResponse
import bg.devlabs.sparrow.ui.base.BasePresenter
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class FundingAccountPresenter @Inject constructor(view: FundingAccountContract.View)
    : BasePresenter<FundingAccountContract.View>(view), FundingAccountContract.Presenter {

    var fundingAccount: Plaid? = null

    override fun onViewBind() {
        compositeDisposable.add(dataManager.getUserScriptsParam(dataManager.userId, dataManager.xAuth)
                .subscribe({ it: Result<UserScriptsParamResponse>? ->
                    if (it?.isError != false) {
                        view?.showErrorToast()
                    } else {
                        it.response()?.body()?.userScriptsParam?.userScriptsParam?.actions?.plaid?.filter { it.accountId != "" }?.get(0)?.let { it: Plaid ->
                            fundingAccount = it
                            view?.onAccountResolved(it)
                        }
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }

    override fun onChangeClick() {
        fundingAccount?.let {

        }
    }

}