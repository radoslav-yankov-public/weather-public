package bg.devlabs.sparrow.ui.corevalues

import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class CoreValuesContract {
    interface View : BaseContract.View{
        fun updateValuesList(data: List<Event>?)
    }

    interface Presenter : BaseContract.Presenter {
        fun onViewBind()
        fun onListLoad()
        fun saveOnboarding()
    }
}
