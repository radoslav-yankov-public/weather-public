package bg.devlabs.sparrow.ui.home


import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.FundAccountFragment
import bg.devlabs.sparrow.ui.base.BasePresenter
import bg.devlabs.sparrow.ui.causes.CausesFragment
import bg.devlabs.sparrow.ui.corevalues.CoreValuesFragment
import bg.devlabs.sparrow.ui.main.MainFragment
import bg.devlabs.sparrow.ui.onboarding.OnboardingFragment
import bg.devlabs.sparrow.utils.CAUSES_COMPLETE
import bg.devlabs.sparrow.utils.CORE_VALUES_COMPLETE
import bg.devlabs.sparrow.utils.SETUP_FUNDING_COMPLETE
import bg.devlabs.sparrow.utils.SIGNUP_COMPLETE
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

class HomePresenter @Inject
constructor(view: HomeContract.View) : BasePresenter<HomeContract.View>(view),
        HomeContract.Presenter {

    override fun onViewBind() {
        if (view == null) {
            return
        }

        if (dataManager.isUserLoggedIn()) {
            compositeDisposable.add(dataManager.getUser(dataManager.userId, dataManager.xAuth)
                    .subscribe({
                        if (it?.response()?.isSuccessful == true) {
                            when (it.response()?.body()?.user?.status) {
                                SIGNUP_COMPLETE -> {
                                    view?.showFragment(FundAccountFragment(), R.string.fund_account_fragment)
                                }
                                SETUP_FUNDING_COMPLETE -> {
                                    view?.showFragment(CausesFragment(), R.string.causes_fragment)
                                }
                                CAUSES_COMPLETE -> {
                                    view?.showFragment(CoreValuesFragment(), R.string.core_values_fragment)
                                }
                                CORE_VALUES_COMPLETE -> {
                                    view?.showFragment(MainFragment(), R.string.main_fragment)
                                }
                            }
                        } else {
                            view?.showFragment(OnboardingFragment(), R.string.onboarding_fragment)
                        }
                    }, {
                        view?.showFragment(OnboardingFragment(), R.string.onboarding_fragment)
                    })
            )

        } else {
            view?.showFragment(OnboardingFragment(), R.string.onboarding_fragment)
        }
    }
}
