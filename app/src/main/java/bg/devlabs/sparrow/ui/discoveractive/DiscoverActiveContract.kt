package bg.devlabs.sparrow.ui.discoveractive

import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class DiscoverActiveContract {
    interface View : BaseContract.View {
        fun onCheckChangeSuccess(id: String, wasChecked : Boolean)
    }

    interface Presenter : BaseContract.Presenter {
        fun onViewBind()
        fun onCheckClick(id: String)
        fun onUncheckClick(id: String)
    }
}
