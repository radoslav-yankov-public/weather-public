package bg.devlabs.sparrow.ui.onboarding

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.WelcomeFragment
import bg.devlabs.sparrow.ui.base.BaseFragment
import bg.devlabs.sparrow.utils.setActionBarTitle
import bg.devlabs.sparrow.utils.setFont
import kotlinx.android.synthetic.main.fragment_onboarding.*


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class OnboardingFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_onboarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupViewPager()
        setupButton()
        text_title.setFont("HelveticaNeue.otf")
        activity?.setActionBarTitle(getString(R.string.title_welcome))
    }

    private fun setupButton() {
        getStartedButton.setOnClickListener {
            showFragment(WelcomeFragment(), R.string.welcome_fragment)
        }
    }

    private fun setupViewPager() {
        val adapter = OnboardingPagerAdapter(childFragmentManager)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float,
                                        positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position > 0) {
                    text_title.setText(R.string.sparrow)
                } else {
                    text_title.setText(R.string.welcome_to_sparrow)
                }
            }
        })
        tabLayout.setupWithViewPager(viewPager, true)
    }
}