package bg.devlabs.sparrow.ui.causes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.data.network.model.userdata.Value
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.corevalues.CoreValuesFragment
import bg.devlabs.sparrow.ui.customview.CauseView
import bg.devlabs.sparrow.ui.plaid.Bank
import bg.devlabs.sparrow.utils.ContextAction
import bg.devlabs.sparrow.utils.onClick
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_causes.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 05.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class CausesFragment : SeekBar.OnSeekBarChangeListener,
        InjectionBaseFragment(),
        CausesContract.View {
    @Inject
    lateinit var presenter: CausesContract.Presenter
    private lateinit var seekBars: ArrayList<SeekBar>
    private var previousProgress = 0
    var contextAction = ContextAction.SIGNUP
    var bank = Bank()
    lateinit var bank2: Bank

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_causes, container, false)
    }

    fun setData(bank: Bank, bank2: Bank) {
//        this.publicToken = publicToken
//        this.accountId = accountId
        this.bank = bank
        this.bank2 = bank2
    }

    override fun onSuccess() {
        when (contextAction) {
            ContextAction.SETTINGS -> activity?.onBackPressed()
            else -> showFragment(CoreValuesFragment(), R.string.core_values_fragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupSeekBarsListeners()
        when (contextAction) {
            ContextAction.SETTINGS -> {
                proceedButton.text = getString(R.string.text_done)
                activity?.setActionBarTitle(getString(R.string.title_portfolio))
            }
            else -> proceedButton.text = getString(R.string.text_proceed)
        }

        proceedButton.onClick {
            if (contextAction == ContextAction.SIGNUP) {
                presenter.saveOnboarding()
            }
            presenter.updateProfileInfo(seekBars.map {
                (it.progress).toString()
            }.toMutableList())
        }

        // Subscribe for remainingProgress value change
        if (bank.publicToken != "") {
            presenter.updateBankInfo(bank, bank2)
        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

    private fun setupSeekBarsListeners() {
        // Add the SeekBar views to the array
        seekBars = arrayListOf(firstCauseView.getSeekBar(), secondCauseView.getSeekBar(),
                thirdCauseView.getSeekBar(), fourthCauseView.getSeekBar(),
                fifthCauseView.getSeekBar(), sixthCauseView.getSeekBar())

        // TODO: Uncomment this when the causes can be get from the server
//        presenter.fetchCauses()

        firstCauseView.setCause(Value(getString(R.string.health_and_disease), "20"))
        secondCauseView.setCause(Value(getString(R.string.poverty_reduction), "20"))
        thirdCauseView.setCause(Value(getString(R.string.education), "20"))
        fourthCauseView.setCause(Value(getString(R.string.environment_and_future), "20"))
        fifthCauseView.setCause(Value(getString(R.string.food_water_and_shelter), "10"))
        sixthCauseView.setCause(Value(getString(R.string.animals), "10"))

        firstCauseView.setOnSeekBarChangeListener(this)
        secondCauseView.setOnSeekBarChangeListener(this)
        thirdCauseView.setOnSeekBarChangeListener(this)
        fourthCauseView.setOnSeekBarChangeListener(this)
        fifthCauseView.setOnSeekBarChangeListener(this)
        sixthCauseView.setOnSeekBarChangeListener(this)
    }

    override fun setupCauseViews(response: UserDataResponse?) {
        val causes = response?.userData?.userData?.values


        firstCauseView.setCause(causes?.get(0))
        secondCauseView.setCause(causes?.get(1))
        thirdCauseView.setCause(causes?.get(2))
        fourthCauseView.setCause(causes?.get(3))
        fifthCauseView.setCause(causes?.get(4))
        sixthCauseView.setCause(causes?.get(5))
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        val causeView = view?.findViewById<CauseView>(seekBar.id)
        causeView?.setPercent(seekBar.progress)
        val newProgress = progress - previousProgress
        val filledSeekBars: List<SeekBar> = if (newProgress > 0) {
            seekBars.minus(seekBar)
                    .filter { it.progress > 0 }
                    .sortedByDescending { it.progress }
        } else {
            seekBars.minus(seekBar).sortedBy { it.progress }
        }
        val size = filledSeekBars.size - 1
        for (i in 0..size) {
            val seekBarsProgressSum = filledSeekBars.sumBy { it.progress } + seekBar.progress
            filledSeekBars[i].progress += 100 - seekBarsProgressSum
            previousProgress = seekBar.progress
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        previousProgress = seekBar.progress
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }
}