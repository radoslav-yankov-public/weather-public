package bg.devlabs.sparrow.ui.plaid

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.ui.SpendingAccountFragment
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.causes.CausesFragment
import bg.devlabs.sparrow.utils.ContextPlaid
import kotlinx.android.synthetic.main.fragment_plaid.*
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class PlaidFragment : InjectionBaseFragment(), PlaidContract.View {
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showPlaidInView(webview = webview)
    }

    @Inject
    lateinit var presenter: PlaidContract.Presenter

    var contextPlaid = ContextPlaid.SIGNUP

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_plaid, container, false)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

    var previousBank = Bank()
    var plaid = Plaid()

    fun isAccountFilledIn() = !(previousBank.publicToken == "" && previousBank.accountId == "")

    fun setData(previousBank: Bank? = null, plaid: Plaid? = null) {
        previousBank?.let {
            this.previousBank = it
        }
        plaid?.let {
            this.plaid = it
        }
    }

    override fun onSuccess(bank1: Bank, bank2: Bank) {
        if (contextPlaid == ContextPlaid.SIGNUP) {
            showFragment(CausesFragment().apply { setData(bank1, bank2) }, R.string.causes_fragment)
        } else {
            activity?.onBackPressed()
        }
    }

    private fun showPlaidInView(webview: WebView) {


        fun generateLinkInitializationUrl(linkOptions: HashMap<String, String>): Uri {
            val builder = Uri.parse(linkOptions["baseUrl"])
                    .buildUpon()
                    .appendQueryParameter("isWebview", "true")
                    .appendQueryParameter("isMobile", "true")
            linkOptions.keys
                    .filter { it != "baseUrl" }
                    .forEach { builder.appendQueryParameter(it, linkOptions[it]) }
            return builder.build()
        }

        fun parseLinkUriData(linkUri: Uri): HashMap<String, String> {
            val linkData = HashMap<String, String>()
            for (key in linkUri.queryParameterNames) {
                linkData.put(key, linkUri.getQueryParameter(key))
            }
            return linkData
        }

        val linkInitializeOptions = HashMap<String, String>()
        linkInitializeOptions.put("key", "679783b20b1307c3147e102df1fd23")
        linkInitializeOptions.put("product", "auth")
        linkInitializeOptions.put("apiVersion", "v2") // set this to "v1" if using the legacy Plaid API
//        linkInitializeOptions.put("env", "development")
        linkInitializeOptions.put("env", "sandbox")
        linkInitializeOptions.put("clientName", "Test App")
        if (isAccountFilledIn() || contextPlaid == ContextPlaid.CONNECTED_ADD) {
            linkInitializeOptions.put("selectAccount", "false")
        } else {
            linkInitializeOptions.put("selectAccount", "true")
        }
        linkInitializeOptions.put("webhook", "http://requestb.in")
        linkInitializeOptions.put("baseUrl", "https://cdn.plaid.com/link/v2/stable/link.html")
        // If initializing Link in PATCH / update mode, also provide the public_token
        // linkInitializeOptions.put("public_token", "PUBLIC_TOKEN")

        // Generate the Link initialization URL based off of the configuration options.
        val linkInitializationUrl = generateLinkInitializationUrl(linkInitializeOptions)

        // Modify Webview settings - all of these settings may not be applicable
        // or necesscary for your integration.
        val webSettings = webview.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.cacheMode = WebSettings.LOAD_NO_CACHE
        WebView.setWebContentsDebuggingEnabled(true)

        // Initialize Link by loading the Link initiaization URL in the Webview
        webview.loadUrl(linkInitializationUrl.toString())

        // Override the Webview's handler for redirects
        // Link communicates success and failure (analogous to the web's onSuccess and onExit
        // callbacks) via redirects.
        webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                // Parse the URL to determine if it's a special Plaid Link redirect or a request
                // for a standard URL (typically a forgotten password or account not setup link).
                // Handle Plaid Link redirects and open traditional pages directly in the  user's
                // preferred browser.
                val parsedUri = Uri.parse(url)
                if (parsedUri.scheme == "plaidlink") {
                    val action = parsedUri.host
                    val linkData = parseLinkUriData(parsedUri)

                    when (action) {
                        "connected" -> {
                            // UserResponse successfully linked

//                            Log.d("Public token: ", linkData["public_token"])
//                            Log.d("Account ID: ", linkData["account_id"])
//                            Log.d("Account name: ", linkData["account_name"])
//                            Log.d("Institution type: ", linkData["institution_type"])
//                            Log.d("Institution name: ", linkData["institution_name"])

                            //TODO CALL TO PRESENTER WITH INFO
                            // Reload Link in the Webview
                            // You will likely want to transition the view at this point.
                            webview.loadUrl(linkInitializationUrl.toString())
                            when (contextPlaid) {
                                ContextPlaid.SIGNUP -> try {
                                    //showFragment(FundAccountFragment(), R.string.fund_account_fragment)
                                    if (isAccountFilledIn()) {

                                        showFragment(CausesFragment().apply {
                                            setData(previousBank,
                                                    Bank(linkData["public_token"] ?: "",
                                                            "",
                                                            linkData["institution_name"] ?: "",
                                                            linkData["institution_id"] ?: "",
                                                            linkData["accounts"] ?: "",
                                                            linkData["account_type"] ?: "",
                                                            linkData["account_name"] ?: ""))
                                        },
                                                R.string.causes_fragment)
                                        //                                    presenter.updateBankInfo(publicToken, accountId, linkData["public_token"] ?: "")
//                                        presenter.updateBankInfo(previousBank, Bank(linkData["public_token"] ?: "",
//                                                "", linkData["institution_name"] ?: "", linkData["institution_id"] ?: "", linkData["accounts"] ?: "", linkData["account_type"] ?: "", linkData["account_name"] ?: ""))
                                    } else {
                                        if (linkData["public_token"].toString().length > 5 && linkData["account_id"].toString().length > 5) {
                                            showFragment(SpendingAccountFragment().apply {
                                                this.setData(
                                                        Bank(linkData["public_token"] ?: "",
                                                                linkData["account_id"] ?: "",
                                                                linkData["institution_name"] ?: "",
                                                                linkData["institution_id"] ?: "",
                                                                linkData["accounts"] ?: "",
                                                                linkData["account_type"] ?: "",
                                                                linkData["account_name"] ?: "")
                                                )
                                            },
                                                    R.string.fund_account_fragment)
                                        }
                                    }
                                } catch (e: Exception) {
                                }
                                ContextPlaid.FUNDING -> if (plaid.id != null) {
                                    Bank(linkData["public_token"] ?: "",
                                            linkData["account_id"] ?: "",
                                            linkData["institution_name"] ?: "",
                                            linkData["institution_id"] ?: "",
                                            linkData["accounts"] ?: "",
                                            linkData["account_type"] ?: "",
                                            linkData["account_name"] ?: "",
                                            plaid.id,
                                            if (contextPlaid == ContextPlaid.CONNECTED_REMOVE) "" else null)
                                            .let {
                                                presenter.updateBankInfo(it, it)
                                            }
                                }
                                ContextPlaid.CONNECTED_ADD -> {
                                    Bank(linkData["public_token"] ?: "",
                                            "", linkData["institution_name"] ?: "",
                                            linkData["institution_id"] ?: "",
                                            linkData["accounts"] ?: "",
                                            linkData["account_type"] ?: "",
                                            linkData["account_name"] ?: "")
                                            .let {
                                                presenter.updateBankInfo(it, it)
                                            }
                                }
                            }
                        }
                        "exit" -> {
                            // UserResponse exited
                            // linkData may contain information about the user's status in the Link flow,
                            // the institution selected, information about any error encountered,
                            // and relevant API request IDs.

//                            Log.d("UserResponse status:", linkData["status"])

                            // The requet ID keys may or may not exist depending on when the user exited
                            // the Link flow.

//                            Log.d("Link request ID: ", linkData["link_request_id"])
//                            Log.d("API request ID: ", linkData["plaid_api_request_id"])

                            //TODO CALL TO PRESENTER WITH INFO
                            activity?.onBackPressed()
                            // Reload Link in the Webview
                            // You will likely want to transition the view at this point.
//                            webview.loadUrl(linkInitializationUrl.toString())
                        }
                        else -> Log.d("Link action detected: ", action)
                    }
                    // Override URL loading
                    return true
                } else if (parsedUri.scheme == "https" || parsedUri.scheme == "http") {
                    // Open in browser - this is most  typically for 'account locked' or
                    // 'forgotten password' redirects
                    view.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                    // Override URL loading
                    return true
                } else {
                    // Unknown case - do not override URL loading
                    return false
                }
            }
        }
    }


}

data class Bank(
        var publicToken: String = "",
        var accountId: String = "",
        var bankName: String = "",
        var institutionId: String = "",
        var accounts: String = "",
        var accountType: String = "",
        var accountName: String = "",
        var id: String? = null,
        var name: String? = null)
