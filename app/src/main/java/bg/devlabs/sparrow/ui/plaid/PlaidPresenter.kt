package bg.devlabs.sparrow.ui.plaid

import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.data.network.model.userscriptsparam.UserScriptsParamResponse
import bg.devlabs.sparrow.ui.base.BasePresenter
import bg.devlabs.sparrow.utils.SETUP_FUNDING_COMPLETE
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class PlaidPresenter @Inject constructor(view: PlaidContract.View)
    : BasePresenter<PlaidContract.View>(view), PlaidContract.Presenter {

    private var lastBanks = mutableListOf<Bank>()

    //presenter has 2 banks because of the logic in the sign up screen. To save only one bank, pass it twice.
    override fun updateBankInfo(bank1: Bank, bank2: Bank) {
        if (lastBanks.size != 0) {
            if (lastBanks[0] == bank1) {
                return
            }
        }
        compositeDisposable.addAll(
                dataManager.setStatus(dataManager.userId, dataManager.xAuth, SETUP_FUNDING_COMPLETE)
                        .subscribe({}, {}),

                dataManager.getUserScriptsParam(dataManager.userId, dataManager.xAuth)
                        .subscribe({ it: Result<UserScriptsParamResponse>? ->
                            if (it?.response()?.isSuccessful == true) {
                                it.response()?.body()?.userScriptsParam?.userScriptsParam?.actions?.plaid?.map { it: Plaid ->
                                    if ((it.publicToken == bank1.publicToken || it.publicToken == bank2.publicToken) && bank1.id != null && bank2.id != null) {
                                        return@subscribe
                                    }
                                }
                            }

                        }, {
                            view?.showErrorToast()
                        }),

                dataManager.updateBankInfo(
                        dataManager.userId,
                        dataManager.xAuth,
                        bank1, bank2)
                        .subscribe({
                            if (it.code().toString().startsWith("2")) {
                                view?.showToast("Success")
                                view?.onSuccess(bank1, bank2)
                            }
                        }, {
                            view?.showErrorToast()
                        })
        )
        lastBanks.clear()
        lastBanks.add(bank1)
        lastBanks.add(bank2)
    }
}