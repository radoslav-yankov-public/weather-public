package bg.devlabs.sparrow.ui.connectedaccounts

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.plaid.PlaidFragment
import bg.devlabs.sparrow.utils.ContextPlaid
import bg.devlabs.sparrow.utils.onClick
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_connected_accounts.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class ConnectedAccountsFragment : InjectionBaseFragment(), ConnectedAccountsContract.View {
    lateinit var adapter: ConnectedAccountsAdapter
    @Inject
    lateinit var presenter: ConnectedAccountsContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_connected_accounts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setActionBarTitle(getString(R.string.text_connected_accounts))
        presenter.onViewBind()
    }

    override fun onAccountsResolved(banks: MutableList<Plaid>) {
        adapter = ConnectedAccountsAdapter(presenter, banks)
        add_bank_button.onClick {
            //            presenter.onAddBankClick()
            showFragment(PlaidFragment().apply {
                contextPlaid = ContextPlaid.CONNECTED_ADD
            }, R.string.plaid_fragment)
        }
        list_bank.adapter = adapter
        list_bank.layoutManager = LinearLayoutManager(context)
    }

    //not needed
    override fun onRemoveClick(bank: Plaid, position: Int) {
        adapter.data?.removeAt(position)
        adapter.notifyDataSetChanged()
//        adapter.notifyItemRemoved(position)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

}