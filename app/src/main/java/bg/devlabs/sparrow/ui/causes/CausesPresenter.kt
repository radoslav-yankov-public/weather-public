package bg.devlabs.sparrow.ui.causes

import bg.devlabs.sparrow.ui.base.BasePresenter
import bg.devlabs.sparrow.ui.plaid.Bank
import bg.devlabs.sparrow.utils.CAUSES_COMPLETE
import bg.devlabs.sparrow.utils.SETUP_FUNDING_COMPLETE
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class CausesPresenter @Inject constructor(view: CausesContract.View)
    : BasePresenter<CausesContract.View>(view), CausesContract.Presenter {

    override fun updateBankInfo(bank: Bank, bank2: Bank) {
        compositeDisposable.addAll(
                dataManager.setStatus(dataManager.userId, dataManager.xAuth, SETUP_FUNDING_COMPLETE)
                        .subscribe({}, {}),
                dataManager.getUserScriptsParam(dataManager.userId, dataManager.xAuth)
                        .subscribe({
                            if (it.response()?.code().toString().startsWith("4")
                                    || it.response()?.body()?.userScriptsParam?.userScriptsParam?.actions?.plaid?.filter {
                                        (it.publicToken == bank.publicToken || it.publicToken == bank2.publicToken)
                                    }?.size == 0) {
                                dataManager.updateBankInfo(
                                        dataManager.userId,
                                        dataManager.xAuth,
                                        bank, bank2)
                                        .subscribe({
                                            if (it.code().toString().startsWith("2")) {
//                        view.showToast("Success")
                                            }
                                        }, {
                                            view?.showErrorToast()
                                        })
                            }
                        }, {
                            view?.showErrorToast()
                        })
        )
    }

    override fun updateProfileInfo(causesList: MutableList<String>) {
        compositeDisposable.add(
                dataManager.updateUser(dataManager.userId, dataManager.xAuth, causesList)
                        .subscribe({
                            if (it.code().toString().startsWith("2")) {
                                view?.onSuccess()
                            } else {
                                view?.showErrorToast()
                            }
                        }, {
                            view?.showErrorToast()
                        })
        )
    }

    override fun saveOnboarding() {
        compositeDisposable.add(
                dataManager.setStatus(dataManager.userId, dataManager.xAuth, CAUSES_COMPLETE)
                        .subscribe({}, {})
        )
    }

    fun fetchCauses() {
        compositeDisposable.add(dataManager.getUserData(dataManager.userId, dataManager.xAuth)
                .subscribe({
                    view?.setupCauseViews(it.response()?.body())
                }, {
                    view?.showErrorToast()
                })
        )
    }
}