package bg.devlabs.sparrow.ui

import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.utils.hideKeyboard


/**
 * Created by Slavi Petrov on 15.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
abstract class KeyboardHideFragment : InjectionBaseFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val gestureDetector = GestureDetectorCompat(context,
                object : GestureDetector.SimpleOnGestureListener() {
                    override fun onSingleTapUp(e: MotionEvent?): Boolean {
                        hideKeyboard()
                        onKeyboardHidden()
                        return super.onSingleTapUp(e)
                    }
                })
        view.setOnTouchListener { v, event ->
            gestureDetector.onTouchEvent(event)
        }
    }

    open fun onKeyboardHidden() {}
}