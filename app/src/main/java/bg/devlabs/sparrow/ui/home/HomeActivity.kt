package bg.devlabs.sparrow.ui.home

import android.net.http.HttpResponseCache
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.base.InjectionBaseActivity
import bg.devlabs.sparrow.ui.profile.ProfileFragment
import bg.devlabs.sparrow.ui.settings.SettingsFragment
import bg.devlabs.sparrow.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject




class HomeActivity : InjectionBaseActivity(), HomeContract.View {

    @Inject
    lateinit var presenter: HomeContract.Presenter
    private var toolbarButtonVisible = false
    private lateinit var toolbarButtonType: ToolbarButtonType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (isNetworkConnected) {
            onNetwork()
        } else {
            onNoNetwork()
        }
        presenter.onViewBind()
//        showFragment(OnboardingFragment(), R.string.onboarding_fragment)
//        showFragment(MainFragment(), R.string.main_fragment)
//        showFragment(CausesFragment(), R.string.main_fragment)
        setupCustomActionBar()
        setupBackStackListener()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_toolbar, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (!toolbarButtonVisible) return true

        val toolbarButton = menu?.findItem(R.id.toolbar_button)
        toolbarButton?.isVisible = toolbarButtonVisible
        when (toolbarButtonType) {
            ToolbarButtonType.PROFILE -> {
                toolbarButton?.setIcon(R.drawable.ic_profile_24dp)
                toolbarButton?.setOnMenuItemClickListener {
                    showFragment(ProfileFragment(), R.string.profile_fragment)
                    return@setOnMenuItemClickListener true
                }
            }
            ToolbarButtonType.SETTINGS -> {
                toolbarButton?.setIcon(R.drawable.ic_settings_24dp)
                toolbarButton?.setOnMenuItemClickListener {
                    showFragment(SettingsFragment(), R.string.settings_fragment)
                    return@setOnMenuItemClickListener true
                }
            }
        }

        return true
    }

    private fun setupCustomActionBar() {
        this.supportActionBar?.setDisplayShowCustomEnabled(true)
        this.supportActionBar?.setDisplayShowTitleEnabled(false)
        val inflater = LayoutInflater.from(this)
        val v = inflater.inflate(R.layout.view_actionbar, null)
//        (v.findViewById(R.id.title) as TextView).setFont("HelveticaNeue.otf")
        val params = ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER_HORIZONTAL)
        this.supportActionBar?.setCustomView(v, params)
//        (v.findViewById(R.id.title) as TextView).text = "Sparrow"
//        this.supportActionBar?.customView = v
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }
    }

    private fun setupBackStackListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount > 0) {
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_layout_container)
                when (currentFragment.tag) {
                    getString(R.string.fund_account_fragment) -> {
                        disableBackPress()
                    }
                    getString(R.string.onboarding_fragment) -> {
                        disableBackPress()
                    }
                    getString(R.string.demo_fragment) -> {
                        disableBackPress()
                    }
                    getString(R.string.main_fragment) -> {
                        disableBackPress()
                    }
                    getString(R.string.welcome_fragment) -> {
                        disableBackPress()
                    }
                    getString(R.string.causes_fragment) -> {
                        disableBackPress()
                    }
                    else -> backPressedEnabled = true
                }
            }
        }
    }

    private fun disableBackPress() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        backPressedEnabled = false
    }

    private fun onNetwork() {
        isNetworkConnected = true
//        showFragment(PlaidFragment(), R.string.plaid_fragment)
    }

    private fun onNoNetwork() {}


    override fun onBackPressed() {
        if (isLastFragmentInBackStack() || !backPressedEnabled) {
            finish()
            return
        }
        supportFragmentManager.findFragmentById(R.id.frame_layout_container).hideKeyboard()
        supportFragmentManager.popBackStack()
    }

    private fun isLastFragmentInBackStack() = supportFragmentManager.backStackEntryCount == 1

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun showFragment(fragment: Fragment, tagResId: Int) {
        showFragment(fragment, getString(tagResId))
    }

    fun showFragment(fragment: Fragment, title: String) {
        val transaction = supportFragmentManager.beginTransaction()//transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left,
                R.anim.slide_in_right, R.anim.slide_out_right)
        transaction.replace(R.id.frame_layout_container, fragment, title)
        transaction.addToBackStack(title)
        transaction.commit()
    }

    override fun onStop() {
        super.onStop()
        val cache = HttpResponseCache.getInstalled()
        cache?.flush()
    }

    companion object {
        private val TAG = HomeActivity::class.java.simpleName
    }

    fun showToolbarButton(toolbarButtonType: ToolbarButtonType) {
        invalidateOptionsMenu()
        toolbarButtonVisible = true
        this.toolbarButtonType = toolbarButtonType
    }

    fun hideToolbarButton() {
        toolbarButtonVisible = false
        invalidateOptionsMenu()
    }
}