package bg.devlabs.sparrow.ui.home


import android.support.v4.app.Fragment
import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

interface HomeContract {

    interface View : BaseContract.View {
        fun showFragment(fragment: Fragment, tagResId: Int)
    }

    interface Presenter : BaseContract.Presenter {
        fun onViewBind()

    }
}
