package bg.devlabs.sparrow.ui.customview

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.widget.SeekBar
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.Cause
import bg.devlabs.sparrow.data.network.model.userdata.Value
import kotlinx.android.synthetic.main.item_cause.view.*


/**
 * Created by Slavi Petrov on 05.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class CauseView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    private var cause: Value? = null

    init {
        inflate(context, R.layout.item_cause, this)
    }

    fun setCause(cause: Value?) {
        this.cause = cause
        causeNameTextView.text = cause?.name
        val value = cause?.value?.toInt() ?: 0
        causeValueSeekBar.progress = value
        setPercent(value)
    }

    fun setOnSeekBarChangeListener(listener: SeekBar.OnSeekBarChangeListener) {
        causeValueSeekBar.setOnSeekBarChangeListener(listener)
        causeValueSeekBar.id = id
    }

    fun getSeekBar(): SeekBar {
        return causeValueSeekBar
    }

    fun setPercent(percent: Int) {
        percentageTextView.text = resources.getString(R.string.percentage, percent)
    }
}