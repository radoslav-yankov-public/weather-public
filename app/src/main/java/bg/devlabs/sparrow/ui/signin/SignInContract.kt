package bg.devlabs.sparrow.ui.signin

import bg.devlabs.sparrow.ui.base.BaseContract


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
interface SignInContract {
    interface View : BaseContract.View {
        fun onSignInSuccess()
        fun onSignInFail()
    }

    interface Presenter : BaseContract.Presenter {
        fun onSignInButtonClicked(email: String, password: String)
        fun onForgotPasswordClicked()
    }
}