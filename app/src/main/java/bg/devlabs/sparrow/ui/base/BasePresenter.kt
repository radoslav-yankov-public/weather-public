package bg.devlabs.sparrow.ui.base

import bg.devlabs.sparrow.data.DataManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 14.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
abstract class BasePresenter<V : BaseContract.View>(var view: V?)
    : BaseContract.Presenter {

    @Inject
    lateinit var compositeDisposable: CompositeDisposable
    @Inject
    lateinit var dataManager: DataManager

    override fun onDetach() {
        view = null
        compositeDisposable.dispose()
    }
}