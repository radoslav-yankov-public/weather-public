package bg.devlabs.sparrow.ui.base


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.utils.hideKeyboard

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

abstract class BaseFragment : Fragment() {

    fun showFragment(fragment: Fragment, titleResId: Int) {
        showFragment(fragment, getString(titleResId))
        hideKeyboard()
    }

    fun showFragment(fragment: Fragment, title: String) {
        val transaction = fragmentManager?.beginTransaction()
        transaction?.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left,
                R.anim.slide_in_right, R.anim.slide_out_right)
        transaction?.replace(R.id.frame_layout_container, fragment, title)
        transaction?.addToBackStack(title)
        transaction?.commit()
        val actionBar = (activity as AppCompatActivity).supportActionBar ?: return
        actionBar.title = title.toUpperCase()
        toggleBackButtonVisibility(actionBar, true)
    }

    private fun toggleBackButtonVisibility(actionBar: ActionBar, visible: Boolean) {
        actionBar.setHomeButtonEnabled(visible)
        actionBar.setDisplayHomeAsUpEnabled(visible)
    }

    fun toggleBackButtonVisibility(visible: Boolean) {
        val actionBar = (activity as AppCompatActivity).supportActionBar ?: return
        toggleBackButtonVisibility(actionBar, visible)
    }

    var isNetworkConnected: Boolean = false
        get() {
            val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).state == NetworkInfo.State.CONNECTED
        }


    /**
     * Shows a network error dialog. Opens wifi settings on accept
     */
    fun showNetworkError() {
        hideProgressDialog()
//        Log.d(TAG, "showNetworkError: 3")
        if (shouldShowNetworkError) {
            showDialog(getString(R.string.error_connection), getString(R.string.error_connection_prompt),
                    Runnable {
                        try {
                            startActivity(Intent(WifiManager.ACTION_PICK_WIFI_NETWORK))
                        } catch (ignored: IllegalStateException) {
                        }
                    }, Runnable { })
            shouldShowNetworkError = false
        }
    }

    private fun showDialog(title: String, desc: String, acceptFunction: Runnable,
                           rejectFunction: Runnable) {
        val context: Context? = context
        if (context != null) {
            AlertDialog.Builder(context)
                    .setTitle(title.toUpperCase())
                    .setMessage(desc)
                    .setPositiveButton(R.string.dialog_yes) { _, _ -> acceptFunction.run() }
                    .setNegativeButton(R.string.dialog_no) { _, _ -> rejectFunction.run() }
                    .create()
                    .show()
        }
    }

    fun showProgressDialog() {
        dialog = ProgressDialog.show(activity, "",
                "Loading. Please wait...", true)
    }

    fun hideProgressDialog() {
        if (dialog != null) {
            if (dialog?.isShowing == true)
                dialog?.hide()
        }
    }

    fun showToast(text: String) {
        (activity as BaseActivity).showToast(text)
    }

    fun showToast(resId: Int) {
        (activity as BaseActivity).showToast(resId)
    }

    fun showErrorToast() {
        showToast(getString(R.string.error_generic))
    }

    fun shareText(text: String) {
        (activity as BaseActivity).shareText(text)
    }

    fun openActivity(activityClass: Class<*>, names: Array<String>, values: Array<String>) {
        (activity as BaseActivity).openActivity(activityClass, names, values)
    }

    companion object {
        private val TAG = BaseFragment::class.java.simpleName
        internal var dialog: ProgressDialog? = null
        private var shouldShowNetworkError = true
    }
}
