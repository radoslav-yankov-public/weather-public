package bg.devlabs.sparrow.ui.settings

import bg.devlabs.sparrow.ui.base.BaseContract


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SettingsContract {
    interface View : BaseContract.View {
        fun onLogoutSuccess()
    }

    interface Presenter : BaseContract.Presenter {
        fun onLogoutClick()

        fun onDeleteClick()


    }
}