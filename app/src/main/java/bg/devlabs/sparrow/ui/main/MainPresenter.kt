package bg.devlabs.sparrow.ui.main

import bg.devlabs.sparrow.data.network.model.values.ValuesResponse
import bg.devlabs.sparrow.ui.base.BasePresenter
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 08.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */

class MainPresenter @Inject
constructor(view: MainContract.View) : BasePresenter<MainContract.View>(view),
        MainContract.Presenter {

    override fun onListLoad() {
//        dataManager.xAuth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YTdkNDkwOTU1ODBmMzJhYjk5ZWFiNjMiLCJhY2Nlc3MiOiJ4LWF1dGgiLCJpYXQiOjE1MTgxNjAxMzd9.6K9Xao3JJqNcfphCb04avvTU0ilqxR0BpjDs2EEzTQc"
//        dataManager.userId = "5a7d49095580f32ab99eab63"
        compositeDisposable.add(dataManager.getCoreValues(dataManager.xAuth)
                .subscribe({ it: Result<ValuesResponse> ->
                    if (it.response()?.isSuccessful == true && it.response()?.body() != null) {
                        view?.updateValuesList(it.response()?.body()?.events)

                    } else {
                        view?.showErrorToast()
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }
}