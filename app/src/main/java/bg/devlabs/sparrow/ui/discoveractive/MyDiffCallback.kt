package bg.devlabs.sparrow.ui.discoveractive

import android.support.v7.util.DiffUtil
import bg.devlabs.sparrow.data.network.model.values.Event
import io.reactivex.annotations.Nullable

/**
 * Created by Simona Stoyanova on 2/8/18.
 * simona@devlabs.bg
 */

class MyDiffCallback(private var newEvents: List<Event>?, private var oldEvents: List<Event>?) :
        DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldEvents?.size ?: 0
    }

    override fun getNewListSize(): Int {
        return newEvents?.size ?: 0
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldEvents?.get(oldItemPosition)?.eventId == newEvents?.get(newItemPosition)?.eventId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldEvents?.get(oldItemPosition) == newEvents?.get(newItemPosition)
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}