package bg.devlabs.sparrow.ui.welcome

import android.content.Intent
import bg.devlabs.sparrow.ui.base.BaseContract
import com.google.android.gms.auth.api.signin.GoogleSignInAccount


/**
 * Created by Slavi Petrov on 20.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
interface WelcomeContract {
    interface View : BaseContract.View {
        fun startActivityForResult(intent: Intent, requestCode: Int)
        fun onGoogleLoginSuccess(account: GoogleSignInAccount?)
    }

    interface Presenter: BaseContract.Presenter {
        fun onGoogleSignInButtonClicked()
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
        fun onFacebookSignInButtonClicked()
    }
}