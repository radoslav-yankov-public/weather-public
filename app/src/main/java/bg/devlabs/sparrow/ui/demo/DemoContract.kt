package bg.devlabs.sparrow.ui.demo

import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class DemoContract {
    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter {
        fun updateBankInfo(publicToken: String, accountId: String)
    }
}
