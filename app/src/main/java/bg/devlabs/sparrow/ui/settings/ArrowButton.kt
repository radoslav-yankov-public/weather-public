package bg.devlabs.sparrow.ui.settings

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

import bg.devlabs.sparrow.R

/**
 * Created by Simona Stoyanova on 2/9/18.
 * simona@devlabs.bg
 */

class ArrowButton : LinearLayout {
    private var leftText: String? = null
    private var mainTextView: TextView? = null

    private fun inflate() {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = inflater.inflate(R.layout.arrow_button, this, true)
        mainTextView = root.findViewById(R.id.main_text_view)

        mainTextView?.text = leftText
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet) {
        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.ArrowButton,
                0, 0)

        try {
            leftText = a.getString(R.styleable.ArrowButton_leftText)
        } finally {
            a.recycle()
        }
        inflate()
    }

    //not used constructor
    constructor(context: Context) : super(context) {}
}
