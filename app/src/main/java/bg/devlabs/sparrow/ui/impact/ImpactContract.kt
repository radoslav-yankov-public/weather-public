package bg.devlabs.sparrow.ui.impact

import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class ImpactContract {
    interface View : BaseContract.View{
        fun onDataFetched(data: UserDataResponse?)
    }

    interface Presenter : BaseContract.Presenter {
        fun fetchImpact()
    }
}
