package bg.devlabs.sparrow.ui.funding

import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.ui.base.BaseContract


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class FundingAccountContract {
    interface View : BaseContract.View {
        fun onAccountResolved(bank: Plaid)

        fun onAccountChanged()
    }

    interface Presenter : BaseContract.Presenter {

        fun onViewBind()

        fun onChangeClick()

    }
}