package bg.devlabs.sparrow.ui.signup

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.FundAccountFragment
import bg.devlabs.sparrow.ui.KeyboardHideFragment
import bg.devlabs.sparrow.utils.disable
import bg.devlabs.sparrow.utils.enable
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_sign_up.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SignUpFragment : KeyboardHideFragment(), SignUpContract.View {
    @Inject
    lateinit var presenter: SignUpContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setActionBarTitle(getString(R.string.app_name))
        setupPasswordEditText()
        setupSignUpButton()
//        setupTermsConditionsTextView()
    }

    private fun setupPasswordEditText() {
        password_edit_text.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signUpButton.callOnClick()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

//    private fun setupTermsConditionsTextView() {
//        val termsConditionsClickableSpan = object : ClickableSpan() {
//            override fun onClick(widget: View?) {
//                Toast.makeText(activity, "terms and conditions", Toast.LENGTH_LONG).show()
//            }
//        }
//        val privacyPolicyClickableSpan = object : ClickableSpan() {
//            override fun onClick(widget: View?) {
//                Toast.makeText(activity, "privacy policy", Toast.LENGTH_LONG).show()
//            }
//        }
//        val termsConditionsText = getString(R.string.text_terms_conditions_span)
//        val privacyPolicyText = getString(R.string.text_privacy_policy_span)
//        termsConditionsTextView.makeLinks(arrayOf(termsConditionsText, privacyPolicyText),
//                arrayOf(termsConditionsClickableSpan, privacyPolicyClickableSpan))
//    }

    private fun setupSignUpButton() {
        signUpButton.setOnClickListener {
            if (Patterns.EMAIL_ADDRESS.matcher(emailTextInputLayout.editText?.text.toString()).matches() &&
                    passwordTextInputLayout.editText?.text.toString().isNotEmpty() &&
                    passwordTextInputLayout.editText?.text.toString().length > 5) {
//                    && nameTextInputLayout.editText?.text.toString().isNotEmpty()) {
//                signUpButton.animate().alpha(0f)
                signUpButton.disable()
                presenter.onSignUpButtonClicked(emailTextInputLayout.editText?.text.toString(),
                        passwordTextInputLayout.editText?.text.toString(), "")
//                        nameTextInputLayout.editText?.text.toString())
            } else {
                showToast(getString(R.string.error_invalid_data))
            }
        }
    }

    override fun onSignUpSuccess() {
        signUpButton.enable()
        showFragment(FundAccountFragment(), R.string.fund_account_fragment)
    }

    override fun onSignUpFail() {
        signUpButton.enable()
        showErrorToast()
    }
}