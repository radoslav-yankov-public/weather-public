package bg.devlabs.sparrow.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.base.BaseFragment
import bg.devlabs.sparrow.ui.causes.CausesFragment
import bg.devlabs.sparrow.ui.plaid.Bank
import bg.devlabs.sparrow.ui.plaid.PlaidFragment
import kotlinx.android.synthetic.main.fragment_spending_account.*


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SpendingAccountFragment : BaseFragment() {
    //    var publicToken = ""
//    var accountId = ""
//    var bankName = ""
    lateinit var bank: Bank
//    lateinit var bank2: Bank

    fun setData(bank: Bank) {
//        this.publicToken = publicToken
//        this.accountId = accountId
//        this.bankName = bankName
        this.bank = bank
//        this.bank2 = bank2
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_spending_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bankName = bank.bankName
        spendingAccountDescTextView.text = getString(R.string.text_spending_account_description)
//        justUseBankButton.text = "${getString(R.string.text_use_space)} ${bank.bankName}"
        justUseBankButton.text = getString(R.string.just_use_bank, bankName)
        justUseBankButton.setOnClickListener {
            showFragment(CausesFragment().apply {
                setData(this@SpendingAccountFragment.bank, this@SpendingAccountFragment.bank)
            }, R.string.causes_fragment)
        }

        differentBankTextView.setOnClickListener {
            showFragment(PlaidFragment().apply {
                setData(bank)
            }, R.string.plaid_fragment)
        }
    }
}