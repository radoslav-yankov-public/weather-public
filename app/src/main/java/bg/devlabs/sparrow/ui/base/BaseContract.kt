package bg.devlabs.sparrow.ui.base

import android.content.res.Resources

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

interface BaseContract {
    interface View {

        fun shareText(text: String)

        fun getString(stringRes: Int): String

        fun showToast(text: String)

        fun showToast(resId: Int)

        fun showErrorToast()

        fun showNetworkError()

        fun showProgressDialog()

        fun hideProgressDialog()

        fun openActivity(activityClass: Class<*>, names: Array<String>, values: Array<String>)

        fun getResources(): Resources
    }

    interface Presenter {
        fun onDetach()
    }
}
