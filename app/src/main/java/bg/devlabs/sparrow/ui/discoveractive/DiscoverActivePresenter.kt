package bg.devlabs.sparrow.ui.discoveractive

import bg.devlabs.sparrow.ui.base.BasePresenter
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class DiscoverActivePresenter @Inject
constructor(view: DiscoverActiveContract.View) : BasePresenter<DiscoverActiveContract.View>(view),
        DiscoverActiveContract.Presenter {

    override fun onCheckClick(id: String) {
        compositeDisposable.add(dataManager.readyValue(id, dataManager.xAuth)
                .subscribe({
                    if (view != null) {
                        view?.onCheckChangeSuccess(id, true)
                    }
                }, {
                    if (view != null) {
                        view?.showErrorToast()
                    }
                })
        )
    }

    override fun onUncheckClick(id: String) {
        compositeDisposable.add(dataManager.unreadyValue(id, dataManager.xAuth)
                .subscribe({
                    if (view != null) {
                        view?.onCheckChangeSuccess(id, false)
                    }
                }, {
                    if (view != null) {
                        view?.showErrorToast()
                    }
                })
        )
    }

    override fun onViewBind() {}

}