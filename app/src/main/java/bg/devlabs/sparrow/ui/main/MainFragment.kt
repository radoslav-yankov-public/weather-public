package bg.devlabs.sparrow.ui.main

import android.arch.lifecycle.MutableLiveData
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.home.HomeActivity
import bg.devlabs.sparrow.ui.home.ToolbarButtonType
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 08.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class MainFragment : InjectionBaseFragment(), MainContract.View {
    @Inject
    lateinit var presenter: MainContract.Presenter
    lateinit var adapter: MainPagerAdapter

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_main, container, false)

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onListLoad()
        (activity as HomeActivity).showToolbarButton(ToolbarButtonType.PROFILE)
    }

    override fun updateValuesList(data: List<Event>?) {
        adapter = MainPagerAdapter(childFragmentManager)
        val events: MutableLiveData<List<Event>> = MutableLiveData()
        events.postValue(data)
        adapter.data = events
        mainViewPager.adapter = adapter
        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        mainTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                changeTabIconColor(tab, R.color.blue)
                when (tab.position) {
                    0 -> activity?.setActionBarTitle(getString(R.string.title_rules))
                    1 -> activity?.setActionBarTitle(getString(R.string.title_impact))
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                tab.icon?.clearColorFilter()
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
//                Log.d("MainFragment", ": ")
            }
        })
        mainTabLayout.setupWithViewPager(mainViewPager)
        for (i in 0 until mainTabLayout.tabCount) {
            mainTabLayout.getTabAt(i)?.setCustomView(R.layout.item_main_tab)
        }
        mainTabLayout.getTabAt(0)?.setIcon(R.drawable.ic_rules_24dp)
        mainTabLayout.getTabAt(1)?.setIcon(R.drawable.ic_impact_24dp)
        mainTabLayout.getTabAt(0)?.let { changeTabIconColor(it, R.color.blue) }
        mainViewPager.currentItem = 0
    }

    fun changeTabIconColor(tab: TabLayout.Tab, colorResId: Int) {
        context?.let {
            tab.icon?.setColorFilter(ContextCompat.getColor(it, colorResId),
                    PorterDuff.Mode.SRC_ATOP)
        }
    }
}