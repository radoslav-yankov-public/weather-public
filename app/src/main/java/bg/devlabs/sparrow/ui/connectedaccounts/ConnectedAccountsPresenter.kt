package bg.devlabs.sparrow.ui.connectedaccounts

import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.ui.base.BasePresenter
import bg.devlabs.sparrow.ui.plaid.Bank
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 13.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class ConnectedAccountsPresenter @Inject constructor(view: ConnectedAccountsContract.View)
    : BasePresenter<ConnectedAccountsContract.View>(view),
        ConnectedAccountsContract.Presenter {
    var connectedAccounts = mutableListOf<Plaid>()

    override fun onViewBind() {
        compositeDisposable.add(dataManager.getUserScriptsParam(dataManager.userId, dataManager.xAuth)
                .subscribe({
                    if (it.isError) {
                        view?.showErrorToast()
                    } else {
                        view?.onAccountsResolved(
                                it?.response()?.body()?.userScriptsParam?.userScriptsParam?.actions?.plaid?.distinctBy { it.institutionId }?.toMutableList()
                                        ?: mutableListOf()
                        )
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }

    override fun onChangeClick(bank: Plaid, position: Int) {
        Bank(id = bank.id, name = "")
                .let {
                    compositeDisposable.add(
                            dataManager.updateBankInfo(dataManager.userId, dataManager.xAuth, it, it)
                                    .subscribe({
                                        if (it.isSuccessful) {
                                            view?.onRemoveClick(bank, position)
                                        } else {
                                            view?.showErrorToast()
                                        }
                                    }, {
                                        view?.showErrorToast()
                                    })
                    )
                }
    }

    override fun onAddBankClick() {
    }
}