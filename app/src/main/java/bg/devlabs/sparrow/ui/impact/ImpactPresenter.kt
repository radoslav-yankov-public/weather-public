package bg.devlabs.sparrow.ui.impact

import bg.devlabs.sparrow.ui.base.BasePresenter
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class ImpactPresenter @Inject
constructor(view: ImpactContract.View) : BasePresenter<ImpactContract.View>(view),
        ImpactContract.Presenter {

    override fun fetchImpact() {
        compositeDisposable.add(dataManager.getUserData(dataManager.userId, dataManager.xAuth)
                .subscribe({
                    if (it.response()?.isSuccessful == true) {
                        view?.onDataFetched(it.response()?.body())
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }
}