package bg.devlabs.sparrow.ui.base

import android.os.Bundle

import dagger.android.AndroidInjection

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

abstract class InjectionBaseActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
    }
}
