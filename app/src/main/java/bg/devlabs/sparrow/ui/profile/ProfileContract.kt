package bg.devlabs.sparrow.ui.profile

import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class ProfileContract {
    interface View : BaseContract.View {
        fun onDataLoaded(data: ProfileData)
    }

    interface Presenter : BaseContract.Presenter {
        fun onViewBind()
        fun onNameClick(name: String)
    }
}
