package bg.devlabs.sparrow.ui.discoveractive


/**
 * Created by Radoslav Yankov on 07.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
import android.animation.ValueAnimator
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.animation.AccelerateDecelerateInterpolator
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.utils.changeHeight
import bg.devlabs.sparrow.utils.load
import bg.devlabs.sparrow.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_value.*


/**
 * Created by Radoslav Yankov on 29.11.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

class DiscoverActiveAdapter(var presenter: DiscoverActiveContract.Presenter,
                            private var data: List<Event>?)
    : RecyclerView.Adapter<DiscoverActiveAdapter.ViewHolder>() {


    var expandedViewHolder: ViewHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val containerView = inflater.inflate(R.layout.item_value, parent, false)
        return ViewHolder(presenter, containerView, this)
    }

//    fun onCheckChangeSuccess(position: Int, wasChecked: Boolean) {
//        data?.get(position)?.status?.ready = !(data?.get(position)?.status?.ready ?: false)
//        data = data?.filterIndexed { index, _ ->
//            index != position
//        }
//        notifyItemRemoved(position)
//    }

    fun checkForExpanded(viewHolder: ViewHolder) {
        if (viewHolder.adapterPosition != expandedViewHolder?.adapterPosition) {
            expandedViewHolder?.animateDescription(forceContract = true)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setup(data?.get(position) ?: Event(), position)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onViewRecycled(holder: ViewHolder?) {
        super.onViewRecycled(holder)
        holder?.isExpanded = false
        holder?.background_long?.changeHeight(1)
    }

    class ViewHolder(val presenter: DiscoverActiveContract.Presenter,
                     override val containerView: View?,
                     val discoverActiveAdapter: DiscoverActiveAdapter)
        : RecyclerView.ViewHolder(containerView), LayoutContainer {

        var longDescriptionSize = 200
        private val longDescriptionDuration = 400L
        var isExpanded = false

        fun animateDescription(forceContract: Boolean = false) {
            discoverActiveAdapter.checkForExpanded(this)
            val start: Int
            val end: Int
            if (background_long.height == 1) {
                isExpanded = false
            }

            if (isExpanded || forceContract) {
                start = if (background_long.height == 1) {
                    1
                } else {
                    longDescriptionSize

                }
                end = 1
            } else {
                discoverActiveAdapter.expandedViewHolder = this
                start = 1
                end = longDescriptionSize
            }

            background_long.setOnClickListener(null)
            isExpanded = !isExpanded
//            delay(longDescriptionDuration) {
//                background_long.onClick { animateDescription() }
//            }


//            if (background_long.visibility == View.VISIBLE) {
//                start = longDescriptionSize
//                end = 0
//                delay(longDescriptionDuration) {
//                    background_long.visibility = View.GONE
//                    background_long.layoutParams.height = 0
//                    background_long.requestLayout()
//                }
//            } else {
////                delay(longDescriptionDuration) {
//                background_long.layoutParams.height = 0
//                background_long.requestLayout()
//                delay(100) {
//                    background_long.visibility = View.VISIBLE
//                }
//
//
////                }
//            }


            ValueAnimator.ofInt(start, end).apply {
                this.duration = longDescriptionDuration
                this.interpolator = AccelerateDecelerateInterpolator()
                addUpdateListener { animation ->
                    background_long.changeHeight((animation.animatedValue as Int).toInt())
                }
                start()
            }
        }

        fun setup(item: Event, currentPosition: Int = 0) {
            calculateBackgroundSize()
            title.text = item.name
            (item.description).let {
                if (it?.size ?: 0 > 0) {
                    description.text = it?.get(0)
                    if (it?.size ?: 0 > 1) {
                        description_long.text = it?.get(1)
                    }
                }
            }
//            longDescriptionSize = background_long.layoutParams.height.toFloat()
            background_status.setBackgroundColor(background_status.resources.getColor(
                    if (item.status?.ready == true) R.color.blue else R.color.gray))
            if (item.status?.ready == true) {
                image.load(R.drawable.ic_clear_black_24dp)
            } else {
                image.load(R.drawable.ic_check_black_24dp)
            }
            background.onClick {
                background_long.visibility = View.VISIBLE
                animateDescription()
            }

            image.onClick {
                if (item.status?.ready == true) {
                    presenter.onUncheckClick(item.eventId ?: "")
                    image.alpha = 0f
                    image.load(R.drawable.ic_check_black_24dp)
                    image.animate().alpha(1f).duration = 100


                    image.setOnClickListener(null)
                } else {
                    presenter.onCheckClick(item.eventId ?: "")
                    image.alpha = 0f
                    image.load(R.drawable.ic_clear_black_24dp)
                    image.animate().alpha(1f).duration = 100


                    image.setOnClickListener(null)
                }
            }
        }

        private fun calculateBackgroundSize() {
//            delay(2000) {
//            container.visibility = View.INVISIBLE
//            container.alpha = 0f
            background_long.viewTreeObserver.addOnGlobalLayoutListener(
                    object : OnGlobalLayoutListener {

                        override fun onGlobalLayout() {
//                            background_long.visibility = View.INVISIBLE
                            longDescriptionSize = if (background_long.height > 1) {
                                background_long.height
                            } else {
                                417
                            }
                            background_long.changeHeight(1)
//                            background_long.visibility = View.GONE
                            background_long.viewTreeObserver.removeGlobalOnLayoutListener(this)

                        }

                    })
//                longDescriptionSize = background_long.height
//             onViewCreated   container.animate().alpha(1f)

//                container.visibility = View.VISIBLE

//            }
        }
    }

    fun setData(filter: List<Event>?) {
        val diffResult = DiffUtil.calculateDiff(MyDiffCallback(filter, this.data))
        diffResult.dispatchUpdatesTo(this)
        this.data = filter
    }

}