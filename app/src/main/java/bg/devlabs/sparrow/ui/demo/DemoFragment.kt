package bg.devlabs.sparrow.ui.demo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class DemoFragment : InjectionBaseFragment(), DemoContract.View {

    @Inject
    lateinit var presenter: DemoPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_fund_account, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        fundAccountLabelTextView.text = ""
//        fundAccountDescTextView.text = "You are logged in"
//        continueButton.text = "You are logged in"
//        if (publicToken != "") {
//            presenter.updateBankInfo(publicToken, accountId)
//        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

//    var publicToken = ""
//    var accountId = ""
//
//    fun setData(publicToken: String, accountId: String) {
//        this.publicToken = publicToken
//        this.accountId = accountId
//    }




}
